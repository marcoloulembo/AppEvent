import Router from './Router';
import RegisterController from './controllers/RegisterController';
import LogInController from './controllers/LogInController';
import Home from './controllers/Home';
import DashboardController from './controllers/DashboardController';
import EventController from './controllers/eventController/EventController';
import EditEventController from './controllers/eventController/EditEventController';
import DeleteEventController from './controllers/eventController/DeleteEventController';

import './index.scss';
import './inputs.scss';

const routes = [
  {
    url: '/',
    controller: Home
  },
  {
    url: '/register',
    controller: RegisterController
  },
  {
    url: '/login',
    controller: LogInController
  },
  {
    url: '/dashboard',
    controller: DashboardController
  },
  {
    url: '/addEvent',
    controller: EventController
  },
  {
    url: '/editEvent/:id',
    controller: EditEventController
  },
  {
    url: '/deleteEvent/:id',
    controller: DeleteEventController
  }
];

new Router(routes);
