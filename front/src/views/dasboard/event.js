export default (item) => {
  const {
    id, title, name, address, description, numero, date, time
  } = item;

  return `
    <div class="p-4 shadow bg-slate-200 rounded-md shadow-xl mx-2 flex flex-col">
      <div>
        <h2 class="text-2xl font-bold text-gray-800">${title}</h2>
        <p class="text-gray-600">${name}</p>
        <p class="text-gray-600">${address}</p>
        <p class="mt-2 text-gray-600">${description}</p>
        <p class="mt-2 text-gray-600">Contact: ${numero}</p>
        <div class="mt-4">
          <span class="text-gray-400">${date}</span>
          <span class="text-gray-400 ml-4">${time}</span>
        </div>
      </div>
      <div class="mt-4 flex space-x-2">
        <a href="/editEvent/${id}" class="bg-blue-500 text-white text-sm px-4 py-2 rounded hover:bg-blue-600" data-id="${id}" data-action="edit">Modifier</a>
        <a href="/deleteEvent/${id}" class="bg-red-500 text-white text-sm px-4 py-2 rounded hover:bg-red-600" data-id="${id}" data-action="delete">Supprimer</a>
      </div>
    </div>
  `;
};
