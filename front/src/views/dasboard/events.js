import event from './event';

export default (events) => `
  <div class="w-sm flex flex-row flex-wrap space-x-4 space-y-4 my-3 justify-between mt-20 rounded-lg overflow-hidden">
    ${events.map((item) => event(item)).join('')}
  </div>
`;
