export default (item) => `
  <div class="flex items-center mx-auto">
    <div id="message"></div>
    <p>Are you sure you want to delete the event "${item.title}"?</p>
    <button id="delete-button" class="bg-red-500 text-white text-sm px-4 py-2 rounded hover:bg-red-600">Confirm Delete</button>
  </div>
`;
