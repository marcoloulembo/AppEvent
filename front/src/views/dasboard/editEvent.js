export default (item) => {
  const {
    title, name, address, description, numero, date, time
  } = item;

  return `
    <div class="flex items-center mx-auto">
        <form class="form" id="editEventForm" method="post">
            <p class="title">Edit event</p>
            <div class="flex">
                <label>
                    <input class="name" name="title" type="text" value="${title}" required>
                    <span>Title</span>
                </label>
                <label>
                    <input class="name" type="text" name="name" value="${name}" required>
                    <span>Name</span>
                </label>
            </div>  
            <label>
                <input class="name" type="text" name="address" value="${address}" required>
                <span>Address</span>
            </label> 
            <label>
                <input class="name" type="text" name="description" value="${description}" required>
                <span>Description</span>
            </label> 
            <label>
                <input class="name" type="tel" name="numero" value="${numero}" required>
                <span>Numéro de téléphone</span>
            </label> 
            <label>
                <input class="name" type="date" name="date" value="${date}" required>
                <span>Date</span>
            </label>
            <label>
                <input class="name" type="time" name="time" value="${time}" required>
                <span>Time</span>
            </label>
            <button type="submit" class="submit">Submit</button>
            <div id="message"></div>
        </form>
    </div>
`;
};
