import eventsView from './events';

const dashboardContainer = (events) => `
  ${eventsView(events)}
  
  `;

export default dashboardContainer;
