class LoginView {
  constructor() {
    this.loginForm = document.querySelector('#app');
  }

  render() {
    this.loginForm.innerHTML = `
    <div class="w-screen flex items-center mx-auto">
      <form class="form" method="get" id="loginForm">
        <p class="title">Login</p>
        <p class="message">Login now and get full access to our app.</p>
        
        <label>
          <input class="email" type="email" name="email" placeholder="" required>
          <span>Email</span>
        </label>
        
        <label>
          <input class="password" type="password" name="password" placeholder="" required>
          <span>Password</span>
        </label>
        
        <button type="submit" class="submit">Submit</button>
        
        <p id="message"></p>
        
        <p class="signin">Don't have an account? <a href="/register">Register</a></p>
      </form>
      </div>
    `;
  }
}

export default LoginView;
