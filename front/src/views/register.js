class RegisterView {
  constructor() {
    this.registerForm = document.querySelector('#app');
    this.run();
  }

  render() {
    return `
    <div class="w-screen flex items-center mx-auto">
      <form class="form" id="registerForm" method="post">
        <p class="title">Register</p>
        <p class="message">Signup now and get full access to our app.</p>
        <div class="flex">
          <label>
            <input class="firstname" name="firstname" type="text" placeholder="" required>
            <span>Firstname</span>
          </label>
          <label>
            <input class="name" type="text" name="name" placeholder="" required>
            <span>Name</span>
          </label>
        </div>  
        <label>
          <input class="email" type="email" name="email" placeholder="" required>
          <span>Email</span>
        </label> 
        <label>
          <input class="numero" type="tel" name="numero" placeholder="" required>
          <span>Numéro de téléphone</span>
        </label> 
        <label>
          <input class="password" type="password" name="password" placeholder="" required>
          <span>Password</span>
        </label>
        <label>
          <input class="password" type="password" name="password" placeholder="" required>
          <span>Confirm password</span>
        </label>
        <button type="submit" class="submit">Submit</button>
        <p class="signin">you already have an account ? <a href="/login">LogIn</a></p>
        <div id="message"></div>
      </form>
      </div>
    `;
  }

  run() {
    this.registerForm.innerHTML = this.render();
    // this.NavBarView.run();
  }
}

export default RegisterView;
