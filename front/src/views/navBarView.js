import img from '../assets/myevents-remove.png';

export default (isUserLoggedIn) => `
  <nav class="navbar flex items-center justify-between p-4 bg-sky-600 shadow-md">
    <div class="flex items-center">
      <div class="logo">
        <img src="${img}" alt="logo" class="h-10">
      </div>
      <button class="navbar-toggler ml-4 lg:hidden focus:outline-none" type="button" onclick="toggleNav()">
        <svg class="w-6 h-6 text-white" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path>
        </svg>
      </button>
    </div>
    <div class="hidden lg:flex lg:items-center" id="navbarNav">
      <ul class="flex text-gray-200 space-x-4">
        <li>
          <a class="hover:text-gray-300" href="/">Home</a>
        </li>
        <li>
          <a class="hover:text-gray-300" href="/dashboard">Dashboard</a>
        </li>
        ${isUserLoggedIn ? `
          <li>
            <a class="hover:text-gray-300" href="#" id="logoutBtn">Logout</a>
          </li>
        ` : `
          <li>
            <a class="hover:text-gray-300" href="/login">LogIn</a>
          </li>
        `}
      </ul>
    </div>
    <div class="lg:hidden hidden flex-col items-center bg-sky-600 p-4 space-y-4" id="mobileNav">
      <ul class="flex flex-col items-center text-gray-200 space-y-4">
        <li>
          <a class="hover:text-gray-300" href="/">Home</a>
        </li>
        <li>
          <a class="hover:text-gray-300" href="/dashboard">Dashboard</a>
        </li>
        ${isUserLoggedIn ? `
          <li>
            <a class="hover:text-gray-300" href="#" id="logoutBtnMobile">Logout</a>
          </li>
        ` : `
          <li>
            <a class="hover:text-gray-300" href="/login">LogIn</a>
          </li>
        `}
      </ul>
    </div>
  </nav>

  <script>
    function toggleNav() {
      const mobileNav = document.getElementById('mobileNav');
      mobileNav.classList.toggle('hidden');
    }

    document.getElementById('logoutBtn')?.addEventListener('click', (e) => {
      e.preventDefault();
      // Log out logic here
    });

    document.getElementById('logoutBtnMobile')?.addEventListener('click', (e) => {
      e.preventDefault();
      // Log out logic here
    });
  </script>
`;
