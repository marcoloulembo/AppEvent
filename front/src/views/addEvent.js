export default () => `
      <div class="flex items-center mx-auto">
        <form class="form" id="addEventForm" method="post">
          <p class="title">Add event</p>
          <div class="flex">
            <label>
              <input class="name" name="title" type="text" placeholder="" required>
              <span>Title</span>
            </label>
            <label>
              <input class="name" type="text" name="name" placeholder="" required>
              <span>Name</span>
            </label>
          </div>  
          <label>
            <input class="name" type="text" name="address" placeholder="" required>
            <span>Address</span>
          </label> 
          <label>
            <input class="name" type="text" name="description" placeholder="" required>
            <span>Description</span>
          </label> 
          <label>
            <input class="name" type="tel" name="numero" placeholder="" required>
            <span>Numéro de téléphone</span>
          </label> 
          <label>
            <input class="name" type="date" name="date" placeholder="" required>
            <span>Date</span>
          </label>
          <label>
            <input class="name" type="time" name="time" placeholder="" required>
            <span>Time</span>
          </label>
          <button type="submit" class="submit">Submit</button>
          <div id="message"></div>
        </form>
      </div>
    `;
