import mariage from '../assets/mariage.jpg';
import anniversaire from '../assets/anniversaire.jpg';
import soirée from '../assets/soirée.jpg';

export default () => `
<h1 class="text-3xl text-gray-200 font-bold text-center m-5">Quel évènement souhaitez-vous ajouter ?</h1>
<div class="flex space-x-4 mt-5">
  <a href="/addEvent" class="relative block max-w-sm bg-indigo-950 w-full mx-auto shadow-lg rounded-lg overflow-hidden hover:shadow-xl transition-shadow duration-300 group">
    <img src="${mariage}" alt="Mariage" class="w-full h-50 object-cover">
    <div class="absolute inset-0 bg-indigo-950 bg-opacity-75 flex items-center justify-center opacity-0 group-hover:opacity-100 transition-opacity duration-300">
      <h2 class="text-2xl font-bold text-gray-200">Mariage</h2>
    </div>
  </a>

  <a href="/addEvent" class="relative block max-w-sm bg-indigo-950 w-full mx-auto shadow-lg rounded-lg overflow-hidden hover:shadow-xl transition-shadow duration-300 group">
    <img src="${anniversaire}" alt="Anniversaire" class="w-full h-50 object-cover">
    <div class="absolute inset-0 bg-indigo-950 bg-opacity-75 flex items-center justify-center opacity-0 group-hover:opacity-100 transition-opacity duration-300">
      <h2 class="text-2xl font-bold text-gray-200">Anniversaire</h2>
    </div>
  </a>

  <a href="/addEvent" class="relative block max-w-sm bg-indigo-950 w-full mx-auto shadow-lg rounded-lg overflow-hidden hover:shadow-xl transition-shadow duration-300 group">
    <img src="${soirée}" alt="Soirée" class="w-full h-50 object-cover">
    <div class="absolute inset-0 bg-indigo-950 bg-opacity-75 flex items-center justify-center opacity-0 group-hover:opacity-100 transition-opacity duration-300">
      <h2 class="text-2xl font-bold text-gray-200">Soirée</h2>
    </div>
  </a>
</div>
`;
