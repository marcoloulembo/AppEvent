import axios from 'axios';
import Cookies from 'js-cookie';
import NavBarView from '../views/navBarView';

class NavBarController {
  constructor() {
    this.NavBarView = document.querySelector('.nav');
  }

  async run() {
    if (!this.NavBarView) {
      // console.error('Navbar element not found');
      return;
    }

    const isUserLoggedIn = await this.checkLoginStatus();
    // console.log('User login status:', isUserLoggedIn);
    this.NavBarView.innerHTML = NavBarView(isUserLoggedIn);
    this.addLogoutEvent();
  }

  async checkLoginStatus() {
    try {
      const response = await axios.get('http://127.0.0.1:81/status');
      // console.log('Status check response:', response.data);
      return response.data.status === 'success';
    } catch (error) {
      // console.error('Error checking login status:', error);
      return false;
    }
  }

  addLogoutEvent() {
    const logoutBtn = document.getElementById('logoutBtn');
    if (logoutBtn) {
      logoutBtn.addEventListener('click', this.handleLogout.bind(this));
    }
  }

  async handleLogout(e) {
    e.preventDefault();

    try {
      const response = await axios.get('http://127.0.0.1:81/logout');

      if (response.data.status === 'success') {
        Cookies.remove('token');
        window.location.href = '/login';
      } else {
        // console.error('Logout failed:', response.data.message);
      }
    } catch (error) {
      // console.error('Error during logout:', error);
    }
  }
}

export default NavBarController;
