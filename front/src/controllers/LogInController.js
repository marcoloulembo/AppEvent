import axios from 'axios';
import Cookies from 'js-cookie';
import LoginView from '../views/login';
import NavBarController from './NavBarController';

class LogInController {
  constructor() {
    this.view = new LoginView();
    this.run();
  }

  run() {
    this.view.render();
    document.getElementById('loginForm').addEventListener('submit', this.handleSubmit.bind(this));
    new NavBarController();
  }

  async handleSubmit(e) {
    e.preventDefault();
    const form = e.target;
    const formData = new FormData(form);

    const data = {
      email: formData.get('email'),
      password: formData.get('password')
    };

    const messageDiv = document.getElementById('message');

    try {
      const response = await axios.post('http://127.0.0.1:81/auth', data);

      if (response.data.status === 'success') {
        const { token } = response.data;
        Cookies.set('token', token);

        // console.log('Token set in cookies:', Cookies.get('token'));

        messageDiv.textContent = 'Login successful!';
        messageDiv.style.color = 'green';

        await new NavBarController().run(); // update the navbar
        window.location.href = '/';
      } else {
        messageDiv.textContent = `Login failed: ${response.data.message}`;
        messageDiv.style.color = 'red';
      }
    } catch (error) {
      messageDiv.textContent = `Error: ${error.message}`;
      messageDiv.style.color = 'red';
    }
  }
}

export default LogInController;
