import axios from 'axios';
import eventsContainer from '../views/dasboard/dashboard';
import navBarView from '../views/navBarView';

const DashboardController = class {
  constructor() {
    this.navBarView = document.querySelector('.nav');
    this.dashboard = document.querySelector('#app');
    this.run();
  }

  navBarRender() {
    return `
    ${navBarView()}
    `;
  }

  render(events) {
    return `
      ${eventsContainer(events)}
    `;
  }

  async getEvents() {
    try {
      const response = await axios.get('http://127.0.0.1:81/dashboard/events', {
        headers: {
          'Content-Type': 'application/json'
        }
      });
      return response.data;
    } catch (error) {
      // console.error('Error fetching members:', error);
      return null;
    }
  }

  async run() {
    const events = await this.getEvents();
    if (events) {
      this.navBarView.innerHTML = await this.navBarRender();
      this.dashboard.innerHTML = await this.render(events);
    }
  }
};

export default DashboardController;
