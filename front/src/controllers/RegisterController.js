import axios from 'axios';
import RegisterView from '../views/register';

class RegisterController {
  constructor() {
    this.view = new RegisterView();
    this.run();
  }

  run() {
    this.view.render();
    document.getElementById('registerForm').addEventListener('submit', this.handleSubmit.bind(this));
  }

  async handleSubmit(e) {
    e.preventDefault();
    const form = e.target;
    const formData = new FormData(form);

    const data = {
      firstname: formData.get('firstname'),
      name: formData.get('name'),
      email: formData.get('email'),
      numero: formData.get('numero'),
      password: formData.get('password')
    };

    const messageDiv = document.getElementById('message');

    try {
      const response = await axios.post('http://127.0.0.1:81/register', data);

      if (response.data.status === 'success') {
        messageDiv.textContent = 'Successful!';
        messageDiv.style.color = 'green';
        window.location.href = '/';
      } else {
        messageDiv.textContent = `Failed: ${response.data.message}`;
        messageDiv.style.color = 'red';
      }
    } catch (error) {
      messageDiv.textContent = `Error: ${error.message}`;
      messageDiv.style.color = 'red';
    }
  }
}

export default RegisterController;
