import NavBarView from '../views/navBarView';
import homeView from '../views/homeView';

const Home = class {
  constructor() {
    this.NavBarView = document.querySelector('.nav');
    this.HomeView = document.querySelector('#app');
    this.run();
  }

  render() {
    return `
      ${NavBarView()}
    `;
  }

  homeRender() {
    return `
      ${homeView()}
    `;
  }

  run() {
    this.NavBarView.innerHTML = this.render();
    this.HomeView.innerHTML = this.homeRender();
  }
};

export default Home;
