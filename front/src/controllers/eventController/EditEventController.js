import axios from 'axios';
import editEvent from '../../views/dasboard/editEvent';
import navBarView from '../../views/navBarView';

class EditEventController {
    constructor() {
        this.eventId = window.location.pathname.split('/')[2]; // Récupère l'ID de l'événement depuis l'URL
        this.editEventView = document.querySelector('#app');
        this.navBarView = document.querySelector('.nav');
        this.run();
    }

    async fetchEvent() {
        try {
            const response = await axios.get(`http://127.0.0.1:81/event/${this.eventId}`);
            return response.data;
        } catch (error) {
            console.error('Error fetching event:', error);
            return null;
        }
    }

    async render(item) {
        return editEvent(item);
    }

    navBarRender() {
        return navBarView();
    }

    async run() {
        const event = await this.fetchEvent();
        if (event) {
            this.navBarView.innerHTML = this.navBarRender();
            this.editEventView.innerHTML = await this.render(event);
            this.form = document.getElementById('editEventForm');
            if (this.form) {
                this.form.addEventListener('submit', this.handleSubmit.bind(this));
            }
        }
    }

    async handleSubmit(e) {
        e.preventDefault();
        const formData = new FormData(this.form);

        const data = {
            title: formData.get('title'),
            name: formData.get('name'),
            address: formData.get('address'),
            description: formData.get('description'),
            numero: formData.get('numero'),
            date: formData.get('date'),
            time: formData.get('time')
        };

        const messageDiv = document.getElementById('message');

        try {
            const response = await axios.put(`http://127.0.0.1:81/event/${this.eventId}`, data, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            if (response.data.status === 'success') {
                messageDiv.textContent = 'Event updated successfully!';
                messageDiv.style.color = 'green';
                window.location.href = '/';
            } else {
                messageDiv.textContent = `Failed: ${response.data.message}`;
                messageDiv.style.color = 'red';
            }
        } catch (error) {
            if (error.response) {
                messageDiv.textContent = `Error: ${error.response.data.message}`;
            } else if (error.request) {
                messageDiv.textContent = 'Error: No response received from server';
            } else {
                messageDiv.textContent = `Error: ${error.message}`;
            }
            messageDiv.style.color = 'red';
        }
    }
}

export default EditEventController;
