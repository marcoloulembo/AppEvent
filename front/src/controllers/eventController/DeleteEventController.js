import axios from 'axios';
import deleteEvent from '../../views/dasboard/deleteEvent';
import navBarView from '../../views/navBarView';

class DeleteEventController {
  constructor() {
    this.eventId = window.location.pathname.split('/')[2]; // Récupère l'ID de l'événement depuis l'URL
    this.deleteEvent = document.querySelector('#app');
    this.NavBarView = document.querySelector('.nav');
    this.run();
  }

  render() {
    return deleteEvent({ id: this.eventId });
  }

  navBarRender() {
    return `
      ${navBarView()}
    `;
  }

  run() {
    this.NavBarView.innerHTML = this.navBarRender();
    this.deleteEvent.innerHTML = this.render();
    this.button = document.getElementById('delete-button');
    if (this.button) {
      this.button.addEventListener('click', this.handleDelete.bind(this));
    }
  }

  async handleDelete() {
    const messageDiv = document.getElementById('message');

    try {
      const response = await axios.delete(`http://127.0.0.1:81/event/${this.eventId}`, {
        headers: {
          'Content-Type': 'application/json'
        }
      });

      if (response.data.status === 'success') {
        messageDiv.textContent = 'Event deleted successfully!';
        messageDiv.style.color = 'green';
        window.location.href = '/';
      } else {
        if (response.data.message) {
          messageDiv.textContent = `Failed: ${response.data.message}`;
        } else {
          messageDiv.textContent = 'Failed: Unknown error';
        }
        messageDiv.style.color = 'red';
      }
    } catch (error) {
      if (error.response) {
        if (error.response.data.message) {
          messageDiv.textContent = `Error: ${error.response.data.message}`;
        } else {
          messageDiv.textContent = 'Error: Unknown error';
        }
      } else if (error.request) {
        messageDiv.textContent = 'Error: No response received from server';
      } else {
        messageDiv.textContent = `Error: ${error.message}`;
      }
      messageDiv.style.color = 'red';
    }
  }
}

export default DeleteEventController;
