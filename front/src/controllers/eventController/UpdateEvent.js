import axios from 'axios';

class UpdateEventController {
  async handleUpdate(id, updatedData) {
    const messageDiv = document.getElementById('message');

    try {
      const response = await axios.put(`http://127.0.0.1:81/event/${id}`, updatedData, {
        headers: {
          'Content-Type': 'application/json'
        }
      });

      if (response.data.status === 'success') {
        messageDiv.textContent = 'Event updated successfully!';
        messageDiv.style.color = 'green';
        window.location.href = '/';
      } else {
        messageDiv.textContent = `Failed to update event: ${response.data.message}`;
        messageDiv.style.color = 'red';
      }
    } catch (error) {
      messageDiv.textContent = `Error: ${error.response?.data?.message || error.message}`;
      messageDiv.style.color = 'red';
    }
  }
}

export default UpdateEventController;
