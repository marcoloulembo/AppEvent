import axios from 'axios';
import AddEventView from '../../views/addEvent';
import NavBarView from '../../views/navBarView';
import EditEventController from './EditEventController';
import DeleteEventController from './DeleteEventController';

class EventController {
  constructor() {
    this.AddEventView = document.querySelector('#app');
    this.NavBarView = document.querySelector('.nav');
    this.editEventController = new EditEventController();
    this.deleteEventController = new DeleteEventController();
    this.run();
  }

  render() {
    return AddEventView();
  }

  navBarRender() {
    return `
      ${NavBarView()}
    `;
  }

  run() {
    this.NavBarView.innerHTML = this.navBarRender();
    this.AddEventView.innerHTML = this.render();
    this.form = document.getElementById('addEventForm');
    if (this.form) {
      this.form.addEventListener('submit', this.handleSubmit.bind(this));
    }
  }

  async handleSubmit(e) {
    e.preventDefault();
    const formData = new FormData(this.form);
  
    const data = {
      title: formData.get('title'),
      name: formData.get('name'),
      address: formData.get('address'),
      description: formData.get('description'),
      numero: formData.get('numero'),
      date: formData.get('date'),
      time: formData.get('time')
    };
  
    console.log('Data sent:', data);
  
    const messageDiv = document.getElementById('message');
  
    try {
      const response = await axios.post('http://127.0.0.1:81/event', data, {
        headers: {
          'Content-Type': 'application/json'
        }
      });
  
      console.log('Server response:', response.data);
  
      if (response.status === 200) {
        messageDiv.textContent = 'Event added successfully!';
        messageDiv.style.color = 'green';
        window.location.href = '/';
        
      } else {
        if (response.data.message) {
          messageDiv.textContent = `Failed: ${response.data.message}`;
        } else {
          messageDiv.textContent = 'Failed: Unknown error';
        }
        messageDiv.style.color = 'red';
      }
    } catch (error) {
      console.error('Request error:', error);
  
      if (error.response) {
        if (error.response.data.message) {
          messageDiv.textContent = `Error: ${error.response.data.message}`;
        } else {
          messageDiv.textContent = 'Error: Unknown error';
        }
      } else if (error.request) {
        messageDiv.textContent = 'Error: No response received from server';
      } else {
        messageDiv.textContent = `Error: ${error.message}`;
      }
      messageDiv.style.color = 'red';
    }
  }
}

export default EventController;
