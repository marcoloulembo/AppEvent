
MyEvents est une application web de gestion d'événements permettant aux utilisateurs de créer, modifier et supprimer leurs propres événements. L'application est construite avec une architecture moderne en utilisant des technologies front-end et back-end robustes.


## Fonctionnalités
- Authentification des Utilisateurs : Inscription, connexion et déconnexion sécurisées.
- Gestion des Événements : Création, modification et suppression des événements.
- Interface Utilisateur Responsive : Conception mobile-first avec Tailwind CSS.
- Notifications : Feedback utilisateur sur les opérations effectuées (succès, erreurs).

## Technologies Utilisées
### Front-end
- JavaScript
- HTML5
- Tailwind CSS : Utilisé pour la conception responsive et les styles CSS.

### Back-end
- Php
- MySQL : Base de données relationnelle pour stocker les informations des utilisateurs et des événements.
- Axios : Bibliothèque pour faire des requêtes HTTP depuis le front-end vers le back-end.

### Outils
- Webpack : Module bundler pour compacter les fichiers JavaScript.
- Eslint : Module bundler pour compacter les fichiers JavaScript.
- Babel : Transpiler pour utiliser les dernières fonctionnalités JavaScript.

## Installation et Configuration
### Prérequis
- Node.js et npm installés sur votre machine.
- MySQL installé et configuré.

### Étapes d'Installation
1. Clonez le dépôt :
   ```bash
   git clone https://gitlab.com/marcoda1/app-event.git
   cd myevents