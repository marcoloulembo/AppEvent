-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : dim. 16 juin 2024 à 19:07
-- Version du serveur : 8.0.36-0ubuntu0.22.04.1
-- Version de PHP : 8.1.2-1ubuntu2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `app_event`
--

-- --------------------------------------------------------

--
-- Structure de la table `evenements`
--

CREATE TABLE `evenements` (
  `id` int NOT NULL,
  `title` varchar(264) NOT NULL,
  `name` varchar(64) NOT NULL,
  `address` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` varchar(300) NOT NULL,
  `numero` varchar(64) NOT NULL,
  `date` varchar(64) NOT NULL,
  `time` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `evenements`
--

INSERT INTO `evenements` (`id`, `title`, `name`, `address`, `description`, `numero`, `date`, `time`) VALUES
(1, 'Mariage', 'Les mariés simplement', '1 rue du mariage', 'Vive les mariés', '123456789', '10/06/2024', '11:00'),
(2, 'Anniversaire', 'Dumbledor', 'Poudlard', 'Vive Voldemor c\'est le goat des antagonistes ☻', '123456789', '24/06/2024', '11:45'),
(3, 'bonjour', 'Le monde', 'tata', 'Wojtek vs Louvar Masterclass', '123456789', '2024-06-20', '22:40'),
(4, 'ça ', 'marche', 'je peux', 'ajouter mes events', '123456789', '2024-06-12', '22:50'),
(5, 'bon dernier test', 'Supprimer ', 'L\'erreur undefine', 'Wojtek vs Louvar Masterclass', '123456789', '2024-06-20', '22:55'),
(6, 'bon', 'jour', 'les ', 'gars', '123456789', '2024-06-22', '10:10'),
(7, 'encore', 'un ', 'event ', 'ajouter', '123456789', '2024-06-14', '10:09'),
(8, 'Velit aut dolorem qu', 'Barbara Newton', 'Laborum Anim qui de', 'Sunt ullam facere u', '+1 (666) 977-4488', '1988-11-09', '02:55'),
(9, 'Consequatur quia aut', 'Harriet Ware', 'Mollit iste laborum ', 'Optio est voluptatu', '+1 (433) 296-4705', '1991-10-15', '14:16'),
(10, 'Consequatur quia aut', 'Harriet Ware', 'Mollit iste laborum ', 'Optio est voluptatu', '+1 (433) 296-4705', '1991-10-15', '14:16'),
(11, 'Ex voluptas molestia', 'Lucy Reeves', 'Tempora voluptatem p', 'Tenetur quod atque n', '+1 (696) 526-8058', '1990-04-20', '15:36'),
(12, 'Debitis quia similiq', 'Hamish Wells', 'Rerum hic quaerat do', 'Qui eius voluptatibu', '+1 (998) 124-1196', '1996-01-31', '13:05'),
(13, 'Quam libero ut esse', 'Kellie Mathis', 'Itaque voluptas qui ', 'Assumenda nemo ut se', '+1 (131) 523-4825', '1998-10-13', '13:42');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `firstname` varchar(264) NOT NULL,
  `name` varchar(264) NOT NULL,
  `email` varchar(264) NOT NULL,
  `numero` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `name`, `email`, `numero`, `password`) VALUES
(1, 'hello', 'world', 'hello@gmail.com', '123456789', '$2y$10$w6dv/Lxb8poRN5OFSIfV6.05fYJRqOt435HRmBohSIsZg35nmtnwq'),
(2, 'marco', 'marco', 'marco@gmail.com', '123456789', '$2y$10$g4R9/Ln75WUZgh.u..T/nOTHGME7tsLNJqdD0XAEz/hH7LTZ8Nse.'),
(3, 'a', 'b', 'ab@gmail.com', '123456789', '$2y$10$psT5Gisl8YA6Yy0Phy2EVuNnDsH.9FymYQ2d0BPckhCIftR6rWyRq'),
(7, 'hey', 'holla', 'hey@gmail.com', '123456789', '$2y$10$6OxlRWdenAMeHYhnONqFQe3V9Ynq8O9Kbjce79fs0snHie6.BMtWi'),
(8, 'TaShya', 'Stewart Huber', 'xyliniged@mailinator.com', '123456789', '$2y$10$xSUTVSWTZJax1dR6d2WZo.AJ69vqMbAUhA3Q0wlc6ufZkTdhzh9MS'),
(9, 'Phyllis', 'Prescott Franks', 'fipugezeny@mailinator.com', '+1 (809) 582-8047', '$2y$10$P0O3s7cgPMVdnOU3vwq50OcyYgy4VN.fFkIIrxxHkYwTQTlrPbbg6'),
(10, 'Holly', 'Rhonda Barber', 'hapeh@mailinator.com', '+1 (736) 621-4999', '$2y$10$m5LkvF8JsL78XM2iFaHbsOZilVIotBaI8bi7XVjr5r3vdyZ9JG6dq'),
(11, 'Josephine', 'Ishmael Buckley', 'jycahejux@mailinator.com', '+1 (292) 806-8586', '$2y$10$F7PVf5bmrXIodi6I5eauL.06YT9oAo54tq0y9HEvgQ2auZy4g1V52');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `evenements`
--
ALTER TABLE `evenements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `evenements`
--
ALTER TABLE `evenements`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
