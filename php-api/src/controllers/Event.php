<?php 

namespace App\Controllers;

use App\Models\EventModel;
use Exception;

class Event extends Controller {
    protected object $event;

    public function __construct($param) {
        $this->event = new EventModel();
        parent::__construct($param);
    }

    protected function setCorsHeaders() {
        header('Access-Control-Allow-Origin: *');
        header('Content-type: application/json; charset=utf-8');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    }

    public function optionsEvent() {  
        $this->setCorsHeaders();
        header('HTTP/1.0 200 OK');
    }

    public function postEvent() {
        $this->setCorsHeaders();

        try {
            $data = json_decode(file_get_contents('php://input'), true);

            if (!$data) {
                throw new Exception('Invalid JSON data');
            }

            $data = [
                'title' => $data['title'],
                'name' => $data['name'],
                'address' => $data['address'],
                'description' => $data['description'],
                'numero' => $data['numero'],
                'date' => $data['date'],
                'time' => $data['time']
            ];

            $this->event->add($data);

            echo json_encode(['status' => 'success', 'message' => 'Event added successfully']);
        } catch (Exception $e) {
            error_log('Error in postEvent(): ' . $e->getMessage());
            http_response_code(500);
            echo json_encode(['status' => 'error', 'message' => 'Internal Server Error']);
        }
    }

    public function putEvent($id) {
        $this->setCorsHeaders();

        try {
            $data = json_decode(file_get_contents('php://input'), true);

            if (!$data) {
                throw new Exception('Invalid JSON data');
            }

            $data = [
                'id' => $id,
                'title' => $data['title'],
                'name' => $data['name'],
                'address' => $data['address'],
                'description' => $data['description'],
                'numero' => $data['numero'],
                'date' => $data['date'],
                'time' => $data['time']
            ];

            $this->event->update($data);

            echo json_encode(['status' => 'success', 'message' => 'Event updated successfully']);
        } catch (Exception $e) {
            error_log('Error in putEvent(): ' . $e->getMessage());
            http_response_code(500);
            echo json_encode(['status' => 'error', 'message' => 'Internal Server Error']);
        }
    }

    public function deleteEvent($id) {
        $this->setCorsHeaders();

        try {
            $this->event->delete($id);

            echo json_encode(['status' => 'success', 'message' => 'Event deleted successfully']);
        } catch (Exception $e) {
            error_log('Error in deleteEvent(): ' . $e->getMessage());
            http_response_code(500);
            echo json_encode(['status' => 'error', 'message' => 'Internal Server Error']);
        }
    }
}
