<?php 

namespace App\Controllers;

use App\Models\UserModel;

class Auth extends Controller {
  protected object $user;

  public function __construct($param) {
    $this->user = new UserModel();
    parent::__construct($param);

    $this->optionsEvent();
    session_start();
  }

  protected function setCorsHeaders() {
    header('Access-Control-Allow-Origin: *');
    header('Content-type: application/json; charset=utf-8');
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE');
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
  }

  public function optionsEvent() {
    $this->setCorsHeaders();
    header('HTTP/1.0 200 OK');
  }

  public function postAuth() {
    $this->setCorsHeaders();

    $email = $this->body['email'];
    $password = $this->body['password'];

    $user = $this->user->getByEmail($email);

    if ($user && password_verify($password, $user['password'])) {
      $_SESSION['user'] = $user;
      return [
        'status' => 'success',
        'message' => 'Login successful',
        'token' => $user
      ];
    }

    return [
      'status' => 'error',
      'message' => 'Invalid email or password'
    ];
  }

  public function getLogout() {
    $this->setCorsHeaders();
    session_unset();
    session_destroy();
    return [
      'status' => 'success',
      'message' => 'Logout successful'
    ];
  }

  public function getStatus() {
    $this->setCorsHeaders();
    if (isset($_SESSION['user'])) {
      return [
        'status' => 'success',
        'message' => 'User is logged in',
        'user' => $_SESSION['user']
      ];
    }
    return [
      'status' => 'error',
      'message' => 'User is not logged in'
    ];
  }
}
